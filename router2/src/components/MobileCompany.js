import React from 'react';

export const MobileCompany = ( { name } ) => {

  return (
    <h1>
      Company &laquo;{name}&raquo;
    </h1>
  )
  ;

};
