import React, { useState } from 'react';

import { HungryGuy } from './components/HungryGuy';
import { Plate } from './components/Plate';

export const App = () => {

    const [apples,setApples]=useState(5);

    const eatAnApple = () => {
        setApples(apples-1);
    };

    return (
        <div>
            <h1>Callback example</h1>
            <HungryGuy eatAnApple={eatAnApple} />
            <br/><br/><hr/><br/>
            <Plate apples={apples} />
        </div>
    );

};


