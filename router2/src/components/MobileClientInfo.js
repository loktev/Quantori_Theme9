import React from 'react';

export const MobileClientInfo = ( { fio, balance } ) => {

  return (
    <h1>
      Client &laquo;{fio}&raquo;, money {balance}
    </h1>
  );

};
