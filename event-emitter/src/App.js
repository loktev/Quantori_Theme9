import React from 'react';

import { HungryGuy } from './components/HungryGuy';
import { Plate } from './components/Plate';

export const App = () => {

    return (
        <div>
            <h1>Event emitter example</h1>
            <HungryGuy />
            <br/><br/><hr/><br/>
            <Plate />
        </div>
    );

};


