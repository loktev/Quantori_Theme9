import { fruitEvents, EAT_APPLES } from "./fruitEvents";

export const fruitStorage={
    apples: 5,
};

fruitEvents.addListener(EAT_APPLES, cnt => {
    fruitStorage.apples-=cnt;
});
