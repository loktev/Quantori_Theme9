import React from 'react';

export const Plate = ( { apples } ) => {
  
  return (
    <div>There are {apples} apples on the plate</div>
  );

}
