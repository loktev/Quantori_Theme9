import React, { useEffect, useState } from 'react';

import { fruitStorage } from "../data/fruitStorage";
import { fruitEvents, APPLES_CHANGED } from "../data/fruitEvents";

export const Plate = () => {
  
  const [apples,setApples]=useState(fruitStorage.apples);

  // we have to define this function and give it a name 
  // to be able to use it both in addListener and removeListener
  const applesChangedHandler = () => {
    setApples(fruitStorage.apples);
  };

  // we use useEffect only to subscribe/unsubscribe to the fruit events
  useEffect( ()=>{

    // every time the number of apples has been changed we want to know about it
    fruitEvents.addListener(APPLES_CHANGED, applesChangedHandler );

    return () => { // effect clearance function
      fruitEvents.removeListener(APPLES_CHANGED, applesChangedHandler );
    };  
  }, [] ); // we have to subscribe only ONCE when the component is being mounted

  return (
    <div>There are {apples} apples on the plate</div>
  );

};
