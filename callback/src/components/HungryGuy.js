import React from 'react';

export const HungryGuy = ( { eatAnApple } ) => {

  return (
    <>
        <input type='button' value='eat an apple' onClick={()=>eatAnApple()} />
    </>
  );

};
