import {EventEmitter} from 'events';

// in the events stream "fruitEvents" there will be all the events to do with fruits.
export const fruitEvents=new EventEmitter(); 

// event "eat apples", payload - the number of apples
export const EAT_APPLES="EAT_APPLES";

// event "the number of apples has been changes", no payload
export const APPLES_CHANGED="APPLES_CHANGED";
