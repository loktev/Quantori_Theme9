import React from 'react';

import { fruitEvents, EAT_APPLES, APPLES_CHANGED } from "../data/fruitEvents";

export const HungryGuy = () => {

  const eatAnApple = () => {
    fruitEvents.emit(EAT_APPLES,1); // eat an apple
    fruitEvents.emit(APPLES_CHANGED); // broadcast message to whom it may concern that the number of apples has been changed
  };

  return (
    <>
        <input type='button' value='eat an apple' onClick={eatAnApple} />
    </>
  );

};
